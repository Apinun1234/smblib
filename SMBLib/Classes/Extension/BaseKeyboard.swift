//
//  BaseKeyboard.swift
//  MyDictionary
//
//  Created by Apinun Wongintawang on 5/9/18.
//  Copyright © 2018 Apinun Wongintawang. All rights reserved.
//

import UIKit

extension SMBLibUVUtil{
    @objc public func dismissKeyboard() {
        self.viewController?.view.endEditing(true)
    }
}
