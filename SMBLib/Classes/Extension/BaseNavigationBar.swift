//
//  BaseNavigationBar.swift
//  Kubota
//
//  Created by Wewillapp03 on 10/15/2559 BE.
//  Copyright © 2559 Wewillapp. All rights reserved.
//

import UIKit
//import SnapKit

@objc public protocol SMBNavDelegte {
    @objc optional func onRightAction()
    @objc optional func onLeftAction()
}

public class SMBLibUVUtil: NSObject{
    public static let shared: SMBLibUVUtil = SMBLibUVUtil.init()
    public var viewController :UIViewController? = nil
    public var navDelegate : SMBNavDelegte? = nil
    
    @objc public func centerMenuAction(sender:UITapGestureRecognizer){
        print("centerMenuAction")
    }
    
    @objc public func rightMenuActon(btnView:UIBarButtonItem){
        dismissKeyboard()
        print("rightActon")
        self.navDelegate?.onRightAction?()
    }
    
    @objc public func leftMenuActon(btnView:UIBarButtonItem){
        dismissKeyboard()
        self.navDelegate?.onLeftAction?()
    }
    
    
    @objc public func onTouchDownTextField(textFiled:UITextField){
        textFiled.resignFirstResponder()
        print("onTouchDownTextField")
    }
    
    @objc public func onCloseSearch(sender:UIGestureRecognizer){
        print("onCloseSearch    ")
    }
    
    @objc public func onDidChangeText(textfield :UITextField){
        print("onDidChangeText")
    }
    
    @objc public func setNavigationCustomTextMiddle(title:String?, titleRight:String?, titleLeft:String?) {
        //detect navigation controller
        guard let _ = self.viewController?.navigationController else{
            NSLog("%@", "navigation is nil. Don't config on navigation controller.")
            return
        }
        
        if let title = title{
            self.setNavCenter(title: title)
        }
        
        if let titleRight = titleRight{
            self.setNavRight(right: titleRight)
        }
        
        if let titleLeft = titleLeft{
            self.setNavLeft(left: titleLeft)
        }
    }
    
    @objc public func setNavCenter(title str: String){
        self.viewController?.navigationItem.title = str
    }
    
    @objc public func setNavRight(right str: String){
        let btn = UIBarButtonItem.init(title: str, style: .done, target: self, action: #selector(self.rightMenuActon(btnView:)))
        self.viewController?.navigationItem.setRightBarButton(btn, animated: true)
    }
    
    @objc public func setNavLeft(left str: String){
        let btn = UIBarButtonItem.init(title: str, style: .done, target: self, action: #selector(self.leftMenuActon(btnView:)))
        self.viewController?.navigationItem.setLeftBarButton(btn, animated: true)
    }
}


