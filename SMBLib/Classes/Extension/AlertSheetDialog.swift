//
//  AlertSheetDialog.swift
//  MyDictionary
//
//  Created by Apinun Wongintawang on 5/9/18.
//  Copyright © 2018 Apinun Wongintawang. All rights reserved.
//

import UIKit

extension UIViewController{
    @objc public func onShowAlertViewActionSheet2Cell(firstTitle str1: String,secondTitle str2: String,firstResponse:@escaping () -> Void, secondResponse:@escaping () -> Void){
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: str1, style: .default)
        { _ in
            firstResponse()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: str2, style: .default)
        { _ in
            secondResponse()
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
}

